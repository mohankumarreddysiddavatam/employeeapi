package com.employee.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmployeeEntity {

	@Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	private Long employeeCode;
	private String employeeName;
	private String employeeGender;
	private String designation;
	private String emailId;
	private String experience;
	private String phoneNumber;
	private String location;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public String getEmployeeGender() {
		return employeeGender;
	}

	public String getDesignation() {
		return designation;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getExperience() {
		return experience;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public void setEmployeeGender(String employeeGender) {
		this.employeeGender = employeeGender;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
