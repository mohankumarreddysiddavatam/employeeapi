package com.employee.dto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRequestDto {

	private List<EmployeeDetailesDto> employeeDetailesDtos = new ArrayList<>();

	public List<EmployeeDetailesDto> getEmployeeDetailesDtos() {
		return employeeDetailesDtos;
	}

	public void setEmployeeDetailesDtos(List<EmployeeDetailesDto> employeeDetailesDtos) {
		this.employeeDetailesDtos = employeeDetailesDtos;
	}

}
