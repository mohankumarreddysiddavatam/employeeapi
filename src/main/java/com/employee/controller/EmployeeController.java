package com.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmployeeDetailesDto;
import com.employee.dto.EmployeeRequestDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.service.DeleteEmployeeService;
import com.employee.service.EmployeeService;
import com.employee.service.FetchEmployeeService;
import com.employee.service.UpdateEmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	UpdateEmployeeService updateEmployeeService;
	
	@Autowired
	DeleteEmployeeService deleteEmployeeService;
	
	@Autowired
	FetchEmployeeService fetchEmployeeService;

	@PostMapping("/addEmployeeList")
	public ResponseEntity<String> addEmployees(@RequestBody EmployeeRequestDto employeeRequestDto) {

		return new ResponseEntity<String>(employeeService.addEmployees(employeeRequestDto), HttpStatus.ACCEPTED);

	}
	
	@PostMapping("/addEmployee")
	public ResponseEntity<String> addEmployee(@RequestBody EmployeeDetailesDto employeeDetailesDto){
		return new ResponseEntity<String>(employeeService.addEmployees(employeeDetailesDto),HttpStatus.ACCEPTED);
		
	}
	
	@PutMapping("/UpdateEmployee")
	public ResponseEntity<String> updateEmployee(@RequestBody EmployeeDetailesDto employeeDetailesDto){
		return new ResponseEntity<String>(updateEmployeeService.updateEmployee(employeeDetailesDto), HttpStatus.ACCEPTED);
		
	}
	@PutMapping("/DeleteEmployee")
	public ResponseEntity<String> deleteEmployee(@RequestParam Long employeeCode){
		return new ResponseEntity<String>(deleteEmployeeService.deleteEmployee(employeeCode), HttpStatus.ACCEPTED);
		
	}
	
	@GetMapping("/fetchEmployee")
	public ResponseEntity<EmployeeResponseDto> fetchEmployee(@RequestParam Long employeeCode){
		return new ResponseEntity<EmployeeResponseDto>(fetchEmployeeService.fetchEmployee(employeeCode),HttpStatus.ACCEPTED);
		
	}
}

