package com.employee.serviceImpl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.EmployeeEntity;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repo.EmployeeRepo;
import com.employee.service.FetchEmployeeService;

@Service
public class FetchEmployeeServiceImpl implements FetchEmployeeService {

	@Autowired
	EmployeeRepo employeeRepo;

	@Override
	public EmployeeResponseDto fetchEmployee(Long employeeCode) {
		// TODO Auto-generated method stub
		Optional<EmployeeEntity>employeeRecord;
		employeeRecord = employeeRepo.findById(employeeCode);
		if(!employeeRecord.isPresent()) {
			throw new EmployeeNotFoundException("Please provide a valid  employee id number");
		}
		EmployeeEntity employeeEntity= employeeRecord.get();
		EmployeeResponseDto employeeResponseDto=new EmployeeResponseDto();
		BeanUtils.copyProperties(employeeEntity, employeeResponseDto);
		return employeeResponseDto;
	}



}
