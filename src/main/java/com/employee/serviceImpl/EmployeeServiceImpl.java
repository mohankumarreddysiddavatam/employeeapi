package com.employee.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeDetailesDto;
import com.employee.dto.EmployeeRequestDto;
import com.employee.entity.EmployeeEntity;
import com.employee.repo.EmployeeRepo;
import com.employee.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepo employeeRepo;

	@Override
	public String addEmployees(EmployeeRequestDto employeeRequestDto) {
		// TODO Auto-generated method stub

		List<EmployeeEntity> employeeEntities = new ArrayList<>();
		for (EmployeeDetailesDto employeeDetailesDto : employeeRequestDto.getEmployeeDetailesDtos()) {
			EmployeeEntity employeeEntity = new EmployeeEntity();
			BeanUtils.copyProperties(employeeDetailesDto, employeeEntity);
			employeeEntities.add(employeeEntity);

		}
		employeeRepo.saveAll(employeeEntities);
		return "Data has been saved successfully";
	}

	@Override
	public String addEmployees(EmployeeDetailesDto employeeDetailesDto) {
		// TODO Auto-generated method stub
		EmployeeEntity employeeEntity=new EmployeeEntity();
		BeanUtils.copyProperties(employeeDetailesDto, employeeEntity);
		employeeRepo.save(employeeEntity);
		return "Data has been saved successfully";
	}

}