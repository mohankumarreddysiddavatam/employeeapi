package com.employee.serviceImpl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeDetailesDto;
import com.employee.entity.EmployeeEntity;
import com.employee.repo.EmployeeRepo;
import com.employee.service.UpdateEmployeeService;

@Service
public class UpdateEmployeeServiceImpl implements UpdateEmployeeService{

	@Autowired
	EmployeeRepo employeeRepo;
	
	@Override
	public String updateEmployee(EmployeeDetailesDto employeeDetailesDto) {
		// TODO Auto-generated method stub
		EmployeeEntity employeeEntity=new EmployeeEntity();
		BeanUtils.copyProperties(employeeDetailesDto, employeeEntity);
		employeeRepo.save(employeeEntity);
		return "Data has been updated successfully";
	}

}
