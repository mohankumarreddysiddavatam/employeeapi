package com.employee.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.entity.EmployeeEntity;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repo.EmployeeRepo;
import com.employee.service.DeleteEmployeeService;

@Service
public class DeleteEmployeeServiceImpl implements DeleteEmployeeService {

	@Autowired
	EmployeeRepo employeeRepo;

	@Override
	public String deleteEmployee(Long employeeCode) {
		// TODO Auto-generated method stub
		Optional<EmployeeEntity> employeeRecord;
		employeeRecord = employeeRepo.findById(employeeCode);
		if (!employeeRecord.isPresent()) {
			throw new EmployeeNotFoundException("Invalid Employee Id");
		}
		EmployeeEntity employeeEntity = employeeRecord.get();
		employeeRepo.delete(employeeEntity);
		return "deleted succesfull";
	}
}
