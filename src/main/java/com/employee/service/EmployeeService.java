package com.employee.service;

import com.employee.dto.EmployeeDetailesDto;
import com.employee.dto.EmployeeRequestDto;

public interface EmployeeService {

	String addEmployees(EmployeeRequestDto employeeRequestDto);

	String addEmployees(EmployeeDetailesDto employeeDetailesDto);

}
