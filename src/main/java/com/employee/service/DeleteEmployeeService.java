package com.employee.service;

public interface DeleteEmployeeService {

	String deleteEmployee(Long employeeCode);

}
