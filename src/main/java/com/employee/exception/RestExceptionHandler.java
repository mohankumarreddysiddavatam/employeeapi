package com.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
@ExceptionHandler(value = EmployeeNotFoundException.class)
public ResponseEntity<ErrorResponse> handleException(EmployeeNotFoundException employeeNotFoundException) {
	ErrorResponse response = new ErrorResponse();
	response.setStatusCode("404 not found");
	response.setStatusMessage(employeeNotFoundException.getMessage());
	return new ResponseEntity<ErrorResponse>(response, HttpStatus.BAD_REQUEST);

}
}
